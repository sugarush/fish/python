set -g python_venv_default python3
set -g python_venv_folder .venv
set -g python_venv_init .venv-init
set -g python_venv_activate .venv-activate
set -g python_venv_deactivate .venv-deactivate
set -g python_venv_requirements .requirements
