if test -n "$VIRTUAL_ENV"
  set_color normal
  printf "(%s) " (basename $VIRTUAL_ENV)
  set_color normal
end
