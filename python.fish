set -l user_base (python3 -c 'import site; print(site.USER_BASE)')

set -l python_bin $user_base/bin

if ! contains $python_bin $PATH
  set -a PATH $python_bin
end

#set -l python_pyenv $HOME/.pyenv/shims

#if ! contains $python_pyenv $PATH
#  set -a PATH $python_pyenv
#end

set -l module_prompt $sugar_install_directory/config/prompt/25-python.fish

if ! test -f $module_prompt
  cp $argv[1]/prompt/25-python.fish $module_prompt
end

set -l module_config $sugar_install_directory/config/python.fish

if ! test -f $module_config
  cp $argv[1]/config/python.fish $module_config
end

source $sugar_install_directory/config/python.fish

set -xg VIRTUAL_ENV_DISABLE_PROMPT true

@sugar-cd '_python-venv-cd'

function _python-venv-cd
  if test -n "$VIRTUAL_ENV"
    if ! string match -qr (dirname $VIRTUAL_ENV) $PWD
      python-venv-deactivate
    end
  end
  if test -f $python_venv_activate
    python-venv-activate
  end
end

function python-venv-init -d 'Initialize a Python virtial environment.'
  set -l python_version $argv[1]
  test -z $python_version; and set -l python_version $python_venv_default

  if ! test -d $python_venv_folder
    $python_version -m venv $python_venv_folder
  end

  if ! test -f $python_venv_init
    echo "set python_version $python_version" > $python_venv_init
  end

  if ! test -f $python_venv_activate
    echo 'set python_module ""' > $python_venv_activate
  end

  if ! test -f $python_venv_deactivate
    echo 'set -e python_module' > $python_venv_deactivate
  end

  python-venv-activate
end

function python-venv-activate -d 'Activate a Python virtual environment.'
  if test -n $VIRTUAL_ENV
    python-venv-deactivate
  end

  if test -d $PWD/$python_venv_folder
    source $PWD/$python_venv_folder/bin/activate.fish
    source $PWD/$python_venv_activate
  else if test -f $PWD/$python_venv_init
    source $PWD/$python_venv_init
    python-venv-init $python_version
    set -e python_version
  end
end

function python-venv-deactivate -d 'Deactivate a Python virtual environment.'
  if functions -q deactivate
    source (dirname $VIRTUAL_ENV)/$python_venv_deactivate
    deactivate
  end
end

function python-venv-requirements-install -d 'Install Python virtual environment requirements.'
  if test -n "$VIRTUAL_ENV"
    pip install --requirement $python_venv_requirements
  else
    sugar-message error 'You are not in a Python virtual environment.'
  end
end
